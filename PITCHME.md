## Final internship presentation

Barre Kevin
2019 Promotions

---

## The Compagny

### AXA

#### DSI / CIO

`Chief Information officer (CIO)`
`Directeur des systèmes d'information`

---

## Location
![IMAGE](assets/axa-terasses.jpg)

---

### Organisation Team

@uml[span-75 bg-white](src/uml/UseCase.puml)

---

Big Data

---

# Organisation Work

@uml[span-75 bg-white](src/uml/gant.puml)

---?color=linear-gradient(90deg, #5384AD 65%, white 35%)

@snap[north-west h4-white]
#### Project Architecture Life Cycle
@snapend

@snap[west span-55]
@ul[list-spaced-bullets text-white text-09]
- Methode
- Waterfall
- Agile
@ulend
@snapend

---

### Project Architecture Pipline

@uml[span-75 bg-white](src/uml/suply.puml)

---

Technologies

---

Cloudera

Hadoop
Spark


---

Azure dev ops
Jira

---

Projects

---

GDPR

---

gdpr:clean

---

gdpr:anno

---

::schema simple

---

::schema simple

---