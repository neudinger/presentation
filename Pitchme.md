The Compagny

AXA
https://gitpitch.com/neudinger/presentation?grs=gitlab
La soutenance se déroule obligatoirement en présentiel, et a lieu sur le Campus Ionis, 95 avenue
Parmentier, 75011 Paris.

> Mettre en valeur la prestation du candidat au sein de l’environnement de stage en détaillant les
tenants et aboutissants humains et techniques.
Le stagiaire ne doit pas hésiter à mettre en avant son expérience personnelle au sein de l’entreprise,
du projet, de l’équipe, et à afficher clairement le résultat positif de sa prestation.
=> Mettre en valeur sa faculté de présentation et de communication.

Les critères de notation lors de la soutenance sont les suivants (ils ne sont pas exhaustifs) :
=> Tenue et comportement / professionnalisme
Tenue du candidat (tenue professionnelle) / Gestuelle / Posture / Façon de s'adresser au jury / Image
professionnelle dégagée.
=> Prestation orale
Elocution / Force de persuasion / Clarté du discours / Dynamisme / Respect du temps imparti / Fil
conducteur et logique lors de la présentation orale / Fluidité du discours en anglais.
=> Adéquation avec l'entreprise
Compréhension des enjeux de l’entreprise, du contexte et de l’impact du projet réalisé / Valeur
apportée du candidat à l’entreprise / Bilan technique et humain de la période de stage.
=> Support PowerPoint
Qualité du support PowerPoint (Ensemble de la présentation, support sans fautes, clarté de la mise
en page, tournure et pertinence des écrits, illustrations etc).
=> Réponses aux questions
Compréhension aux questions posées par le jury / Pertinence et clarté des réponses.

---

AXA Final internship

---

The Compagny

AXA

---

DSI / CIO

Chief Information officer (CIO)
French Area

---

Organisation Team

UML...

---

Big Data

---

Organisation Work

---

Project Architecture Life Cycle

Waterfall / Agile
Methode

---

Project Architecture Pipline

---

Technologies

---

Cloudera

Hadoop
Spark


---

Azure dev ops
Jira

---

Projects

---

GDPR

---

gdpr:clean

---

gdpr:anno

---

::schema simple

---

::schema simple

---



Thanks
Question

---


datazone.png